<?php

use \mywishlist\models\Liste;
use \mywishlist\models\Prestation;
use \mywishlist\controler\ListParticipationControler;
use \mywishlist\controler\ListCreateControler;
use \mywishlist\controler\CompteControler;
use \mywishlist\vue\VuePages;
use \Illuminate\Database\Capsule\Manager as DB;

require_once 'vendor/autoload.php';

session_start();


$db = new DB();
$db->addConnection( [
 'driver' => 'mysql',
 'host' => 'localhost',
 'database' => 'kratz14u',
 'username' => 'kratz14u',
 'password' => '12345',
 'charset' => 'utf8',
 'collation' => 'utf8_unicode_ci',
 'prefix' => ''
] );
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();
//echo "coucou";

$app->get('/',function(){
  $c2 = new CompteControler();
  $c2->afficherButtonsAccueil();
});

$app->get('/authentification',function(){
  echo 'test';
  $c = new CompteControler();
  $c->formulaireConnexion();
});

$app->get('/inscription', function(){
  $c = new CompteControler();
  $c->afficherLesComptes();
});

$app->get('/hello/world',function(){
  echo "hello world !";
});


$app->get('/listes',function(){
  $c2 = new ListCreateControler();
  $c2->creerListe();

});

$app->post('/listes',function(){
  $c2 = new ListParticipationControler();
  $c2->afficherListeDesListes();

});

$app->get('/liste/:idliste',function($idliste){
  $c = new ListParticipationControler();
  $c->afficherItemsDeLaListe($idliste);
});

$app->post('/liste/:idliste',function($idliste){
  $c = new ListCreateControler();
  $c->ajouterItem($idliste);
});

$app->get('/item/:id',function($id){
  $c = new ListParticipationControler();
  $c->afficherItem($id);
});

$app->post('/item/:id',function($id){
  $c = new ListParticipationControler();
  $c->reserver($id);
});

 $app->post('/compte', function(){
   $c = new CompteControler();
   $c->creerCompteControl();
 });

 $app->get('/compte', function(){
   $c = new CompteControler();
   $c->afficherLesComptes();
 });

 $app->post('/listes/modif',function(){
   $c2 = new ListParticipationControler();
   $c2->modifierListe($_POST['numeroListe']);

 });

 $app->get('/panier/:id',function($id){
  $c = new ListParticipationControler();
  $c->afficherPanier($id);
});

$app->get('/accueil',function(){
  $vue = new VuePages();
  $vue->render(1);
});

$app->get('/prestations',function(){
  $vue = new VuePages();
  $vue->render(2);
});

$app->get('/panier',function(){
  $vue = new VuePages();
  $vue->render(3);
});

$app->get('/contact',function(){
  $vue = new VuePages();
  $vue->render(4);
});

$app->run();
