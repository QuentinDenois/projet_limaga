<?php

namespace mywishlist\controler;

class ListCreateControler{

  public function creerListe(){

    $nvListe = new \mywishlist\models\Liste();
    $nvListe->titre = $_POST['titreListe'];
    $nvListe->description = $_POST['descrListe'];
    $nvListe->expiration = $_POST['expiration'];
    $nvListe->save();

    $liste = \mywishlist\models\Liste::where("user_id","=",$_SESSION['user_id'])->get()->toArray();
    $vue = new \mywishlist\vue\VueLocation($liste);
    $vue->render(0);

  }

  public function ajouterItem($idliste){
    $liste = \mywishlist\models\Liste::find($idliste);

    if($liste->user_id == $_SESSION['user_id']){
      $item = new \mywishlist\models\Prestation();
      $item->liste_id = $idliste;
      $item->nom = $_POST['nom'];
      $item->descr = $_POST['description'];
      $item->tarif = $_POST['prix'];
      $item->save();
    }

    $liste = \mywishlist\models\Liste::find($idliste)->toArray();
    $vue = new \mywishlist\vue\VuePanier([$liste]);
    $vue->render(1);

  }

}
