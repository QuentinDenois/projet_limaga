<?php

use \mywishlist\models\Liste;
use \mywishlist\models\Prestation;
use \mywishlist\vue\VueLocation;
use \mywishlist\vue\VuePanier;

namespace mywishlist\controler;

class ListParticipationControler{

  public function listes(){
      echo "listes";
  }

  public function afficherListeDesListes(){
    $liste = \mywishlist\models\Liste::all()->toArray();
    $vue = new \mywishlist\vue\VuePanier($liste);
    $vue->render(0);
  }

  public function afficherItemsDeLaListe($idliste){
    $liste = \mywishlist\models\Liste::find($idliste)->toArray();
    $vue = new \mywishlist\vue\VuePanier([$liste]);
    $vue->render(1);
  }

  public function afficherItem($id){
    $item = \mywishlist\models\Prestation::find($id)->toArray();
    $vue = new \mywishlist\vue\VuePanier([$item]);
    $vue->render(2);
  }

  public function reserver($id){
    $item = \mywishlist\models\Prestation::find($id);
    $item->participant = $_POST['participant'];
    $item->message = $_POST['message'];
    $item->save();
    $liste = $item->liste->toArray();
    $vue = new \mywishlist\vue\VuePanier([$liste]);
    $vue->render(1);
  }

  public function modifierListe($idliste){
      $liste = \mywishlist\models\Liste::find($idliste);
      if(isset($_POST['titreListe'])){
        $liste->titre = $_POST['titreListe'];
      }
      if(isset($_POST['descrListe'])){
        $liste->description = $_POST['descrListe'];
      }
      if(isset($_POST['expiration'])){
        $liste->expiration = $_POST['expiration'];
      }
      $liste->save();

      $liste = \mywishlist\models\Liste::all()->toArray();
      $vue = new \mywishlist\vue\VuePanier($liste);
      $vue->render(0);
  }
  
  public function afficherPanier($id){
    $panier = \mywishlist\models\Panier::find($id);
    $vue = new \mywishlist\vue\VuePanier([$panier]);
    $vue->render(3);
  }
}
