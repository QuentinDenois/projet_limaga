<?php

namespace mywishlist\controler;

class CompteControler{

  public function creerCompteControl(){
    $util = new \mywishlist\models\Compte();

    $login=$_POST['Login'];
    $mdp=$_POST['MotdePasse'];
    $hash=password_hash($mdp, PASSWORD_DEFAULT, ['cost'=> 12] );
    //$util = new \mywishlist\models\Compte($login, $hash);
    $util->login=$login;
    $util->mdp=$hash;
    $util->save();

    $this->afficherLesComptes();
  }

  public function authentifierCompte(){

    $login=$_POST['Login'];

    $util =\mywishlist\models\Compte::where("login","=",$login)->first();


    $mdp=$_POST['MotdePasse'];
    if (password_verify($mdp, $util->mdp)){
      $_SESSION = ['user_id'=> $util->id_compte];
      echo 'mot de passe correct';
    }
    //$util = new \mywishlist\models\Compte($login, $hash);
    $liste = \mywishlist\models\Liste::where("user_id","=",$_SESSION['user_id'])->get()->toArray();
    $vuec = new \mywishlist\vue\VueCreateur($liste);
    $vuec->render(0);


  }

  public function formulaireConnexion(){
    $tab=[];
    $vueC = new \mywishlist\vue\VueCompte($tab);
    $vueC->render(2);
  }

  public function afficherButtonsAccueil(){
    $tab=[];
    $vueC = new \mywishlist\vue\VueCompte($tab);
    $vueC->render(0);
  }

  public function afficherLesComptes(){
    $comptes = \mywishlist\models\Compte::all()->toArray();
    $vueC = new \mywishlist\vue\VueCompte($comptes);
    $vueC->render(3);

  }






}
