<?php
namespace mywishlist\vue;

class VuePages{

  public function __construct(){
  }

  public function page_accueil(){
    $res = '<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
            <meta  charset="utf-8">
    		<link rel=\'stylesheet\' href=\'CSS/projet.css\'>
        </head>

        <body>
            <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

    		<hr/>




    		<section id="nav">

    			<ul>
    				<li><a class="active" href="accueil"> ACCUEIL </a></li>
    				<li><a href="prestations"> PRESTATIONS </a></li>
    				<li><a href="contact"> CONTACT </a></li>
    				<li><a href="panier"> PANIER </a></li>

            <li><a></a></li>
            <li><a></a></li>
            <li><a></a></li>


            <li><a href="authentification"> SE CONNECTER </a></li>
            <li><a href="inscription"> S\'INSCRIRE </a></li>


    			</ul>

    			<div id="intro">
                <br/>
                <br/>
    			<br/>
    			<br/>

    			<span class=\'texteAccueil\'>  <u><strong>Le complexe aquatique dispose de plusieurs infrastructures :</strong></u> <br/>
            - un bassin sportif de 25 mètres<br/>
            - un petit bain de 8x10 mètres<br/>
            - un toboggan de 35 mètres<br/>
            - une terrasse de 50 m2<br/>

            Les petits et les grands y trouveront leur bonheur, pour un moment de détente, de sport ou d\'amusement ! <br/>  <br/>
            <u><strong>Infos pratiques :</strong></u> <br/>
            - Le port du bonnet de bain est obligatoire <br/>
            - Pour monsieur, seul le slip de bain ou le boxer en élasthanne est autorisé. Pour madame, seul le maillot de bain 1 ou 2 pièces est autorisé <br/>
            - Les enfants de moins de 18 ans doivent être sous la surveillance d\'un adulte <br/>
            - Le port de bijoux est déconseillé, la direction et les employés du complexe ne seront pas responsables en cas de perte <br/>
          </span>

          <span class=\'pool\'>
            <img src="images/bassin.jpg" alt="Bassin sportif"/>
          </span>
    			</div>

    		</section>

    		<hr/>

            <footer>
    			<span>Site WEB LIMAGA 2017/2018 </span><strong>Gabriel DAUBENFELD && Quentin DENOIS && Juliette KRATZ</strong>
    		</footer>
        </body>

    </html>';

    return $res;
  }

  private function page_prestations(){
    $res = '<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
            <meta  charset="utf-8">
    		<link rel=\'stylesheet\' href=\'CSS/projet.css\'>
        </head>

        <body>
            <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

    		<hr/>




    		<section id="nav">

    			<ul>
            <li><a class="active" href="accueil"> ACCUEIL </a></li>
            <li><a href="prestations"> PRESTATIONS </a></li>
            <li><a href="contact"> CONTACT </a></li>
            <li><a href="panier"> PANIER </a></li>

            <li><a></a></li>
            <li><a></a></li>
            <li><a></a></li>


            <li><a href="authentification"> SE CONNECTER </a></li>
            <li><a href="inscription"> S\'INSCRIRE </a></li>

    			</ul>

    <br/><br/>
    				<article id="intro">

    					<p id="p1"><b>Le complexe nautique LIMAGA vous propose différentes prestations : </b><br/><br/>

    				</article>

    				<article id="prestations">



    				</article>

    			 </p>

            </section>

    		<hr/>

            <footer>

    			<span>Projet Documents et Interfaces Web 2016 </span><strong>Gabriel DAUBENFELD et Juliette KRATZ</strong>

    		</footer>

        </body>

    </html>
';

    return $res;
  }

  public function page_panier(){
    $res = '<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
            <meta  charset="utf-8">
    		<link rel=\'stylesheet\' href=\'CSS/projet.css\'>
        </head>

        <body>
            <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

    		<hr/>




    		<section id="nav">

    			<ul>
            <li><a class="active" href="accueil"> ACCUEIL </a></li>
            <li><a href="prestations"> PRESTATIONS </a></li>
            <li><a href="contact"> CONTACT </a></li>
            <li><a href="panier"> PANIER </a></li>

            <li><a></a></li>
            <li><a></a></li>
            <li><a></a></li>


            <li><a href="authentification"> SE CONNECTER </a></li>
            <li><a href="inscription"> S\'INSCRIRE </a></li>

    			</ul>

    				<article id="intro">

    					<p id="p1"><br/><br/>

    				</article>

    				<article id="algo">



    				</article>

    				<article id= "lexique">



    			 </p>

            </section>
    		<hr/>
            <footer>

    			<span>Projet Documents et Interfaces Web 2016 </span><strong>Gabriel DAUBENFELD et Juliette KRATZ</strong>

    		</footer>

        </body>

    </html>
';

    return $res;
  }

  public function page_connection(){
    $res = '<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
            <meta  charset="utf-8">
    		<link rel=\'stylesheet\' href=\'CSS/projet.css\'>
        </head>

        <body>
            <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

    		<hr/>




    		<section id="nav">

    			<ul>
    				<li><a class="active" href="accueil"> ACCUEIL </a></li>
    				<li><a href="prestations"> PRESTATIONS </a></li>
    				<li><a href="contact"> CONTACT </a></li>
    				<li><a href="panier"> PANIER </a></li>

            <li><a></a></li>
            <li><a></a></li>
            <li><a></a></li>


            <li><a href="authentification"> SE CONNECTER </a></li>
            <li><a href="inscription"> S\'INSCRIRE </a></li>


    			</ul>

    			<div id="intro">
                <br/>
                <br/>
    			<br/>
    			<br/>

    			<span class=\'texteAccueil\'>  <u><strong>Le complexe aquatique dispose de plusieurs infrastructures :</strong></u> <br/>
            - un bassin sportif de 25 mètres<br/>
            - un petit bain de 8x10 mètres<br/>
            - un toboggan de 35 mètres<br/>
            - une terrasse de 50 m2<br/>

            Les petits et les grands y trouveront leur bonheur, pour un moment de détente, de sport ou d\'amusement ! <br/>  <br/>
            <u><strong>Infos pratiques :</strong></u> <br/>
            - Le port du bonnet de bain est obligatoire <br/>
            - Pour monsieur, seul le slip de bain ou le boxer en élasthanne est autorisé. Pour madame, seul le maillot de bain 1 ou 2 pièces est autorisé <br/>
            - Les enfants de moins de 18 ans doivent être sous la surveillance d\'un adulte <br/>
            - Le port de bijoux est déconseillé, la direction et les employés du complexe ne seront pas responsables en cas de perte <br/>
          </span>

          <span class=\'pool\'>
            <img src="images/bassin.jpg" alt="Bassin sportif"/>
          </span>
    			</div>

    		</section>

    		<hr/>

            <footer>
    			<span>Site WEB LIMAGA 2017/2018 </span><strong>Gabriel DAUBENFELD && Quentin DENOIS && Juliette KRATZ</strong>
    		</footer>
        </body>

    </html>';


    return $res;
  }

  public function page_contact(){
    $res = '<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
            <meta  charset="utf-8">
    		<link rel=\'stylesheet\' href=\'CSS/projet.css\'>
        </head>

        <body>
            <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

    		<hr/>




    		<section id="nav">

    			<ul>
            <li><a class="active" href="accueil"> ACCUEIL </a></li>
            <li><a href="prestations"> PRESTATIONS </a></li>
            <li><a href="contact"> CONTACT </a></li>
            <li><a href="panier"> PANIER </a></li>

            <li><a></a></li>
            <li><a></a></li>
            <li><a></a></li>


            <li><a href="authentification"> SE CONNECTER </a></li>
            <li><a href="inscription"> S\'INSCRIRE </a></li>

    			</ul>
    <br>
    <br>
    <br>
    				<article id="intro">

    					<p id="p1"> <strong>Page en construction</strong>
    				</article>


            <footer>

    			<span>Projet Documents et Interfaces Web 2016 </span><strong>Gabriel DAUBENFELD et Juliette KRATZ</strong>

    		</footer>

        </body>

    </html>
';

    return $res;
  }

  public function page_inscription(){
    $res = '';

    return $res;
  }





  public function render(int $selecteur) {
  switch ($selecteur) {
    case 1 :
    $content = $this->page_accueil();
    break;

    case 2 :
    $content = $this->page_prestations();
    break;

    case 3 :
    $content = $this->page_contact();
    break;

    case 4 :
    $content = $this->page_panier();
    break;



 }
echo $content;
}

}
