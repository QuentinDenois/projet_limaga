<?php
namespace mywishlist\vue;

class VueLocation{

  private $tableau;

  public function __construct($tab){
    $this->tableau = $tab;
  }

  public function afficherListesDuCreateur(){

    $res='<section>';
    foreach($this->tableau as $t){
      $res.= 'Titre de la liste = '.$t['titre'].'<br>Description de la liste = '.$t['description'].'<br>------<br>';
    }
    $res.='</section>';
    $res .= '<form id="creerListe" method="POST" action="./listes">
    <fieldset>
      <legend>Creation d une nouvelle liste</legend>
      <label for="f1_name">Titre de la nouvelle liste : </label>
      <input type="text" id="f1_name" name="titreListe" required>
      <label for="f2_name">Description :</label>
      <input type="text" id="f2_name" name="descrListe" required>
      <label for="f3_name">Date d expiration :</label>
      <input type="date" id="f3_name" name="expiration" required>

        <button type="submit">Creer la liste</button></form>
    </fieldset>';


    return $res;
  }

  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->afficherListesDuCreateur();
    break;
  }
$html =<<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='../CSS/projet.css'>
    </head>

    <body>
        <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

		<hr/>




		<section id="nav">

			<ul>
				<li><a href="projet.html"> ACCUEIL </a></li>
				<li><a class="active" href="prestations.html"> PRESTATIONS </a></li>
				<li><a href="contact.html"> CONTACT </a></li>
				<li><a href="panier.html"> PANIER </a></li>

			</ul>

<br/><br/>
				<article id="intro">

					<p id="p1"><b>Le complexe nautique LIMAGA vous propose différentes prestations : </b><br/><br/>

				</article>

				<article id="prestations">
          <ul><input type="button" value="E-billet"></ul><br>
          <ul><input type="button" value="E-abonnement"></ul><br>
          <ul><a href="location.html"><input type="button" value="Location de matériel"></ul><br>
          <ul><input type="button" value="Achat de produits dérivés"></ul><br>

				</article>

			 </p>

        </section>

		<hr/>

        <footer>

			<span>Projet Documents et Interfaces Web 2016 </span><strong>Gabriel DAUBENFELD et Juliette KRATZ</strong>

		</footer>

    </body>

</html>

END;
echo $html;
}


}
