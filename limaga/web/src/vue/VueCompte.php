<?php
namespace mywishlist\vue;

class VueCompte{

  private $tableau;

  public function __construct($tab){
    $this->tableau = $tab;
  }

  public function formulaireCompte(){
    $res = '
    <form id="f1" method="POST" action="./compte">
      <fieldset> <legend> Création de compte</legend>
        <input type="text" name="Login" placeholder="Login" >
        <input type="password" name="MotdePasse" placeholder="Mot de passe" >

        <button type="submit">valider</button>
      </fieldset>
    </form>';

    return $res;
  }

  private function afficherComptes(){
    $res='<section>'. 'Login déjà existants = <br>';
    foreach($this->tableau as $t){
      $res.= $t['login'].'<br>';
    }
    $res.='</section>';

    return $res;
  }

  public function authentificationCompte(){
    $res = '


    <form id="monform" method="POST" action="./listes">

      <fieldset> <legend>  Authentification </legend>
          <label for="identif">Identifiant <input id = "identif" type="text" name="Identifiant" placeholder="Login" ></label>  <br>
          <label for="mdp">Mot de passe <input id = "mdp" type="password" name="MotdePasse" placeholder="Mot de passe" ></label><br>
        <br>

        <button class="button">Connexion</button>
      </fieldset>

    </form>
    ';
    return $res;
  }

  public function buttonsAccueil(){
    $res = '<input type="button" value="Se connecter" name="submit" onclick= "window.location = \'authentification\'">';
    $res .= '<input type="button" value="S\'inscrire" name="submit" onclick= "window.location = \'inscription\'">';
    return $res;
  }





  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->buttonsAccueil();
    break;

   case 3 :
   $content = $this->afficherComptes();
   $content.= $this->formulaireCompte();
   break;

   case 2 :
   $content = $this->authentificationCompte();
   break;

 }


$html = <<<END
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>COMPLEXE AQUATIQUE DE LIMAGA</title>
        <meta  charset="utf-8">
		<link rel='stylesheet' href='./CSS/projet.css'>
    </head>

    <body>
        <header> <strong>COMPLEXE AQUATIQUE DE LIMAGA </strong> </header>

		<hr/>




		<section id="nav">

			<ul>
        <li><a class="active" href="accueil"> ACCUEIL </a></li>
        <li><a href="prestations"> PRESTATIONS </a></li>
        <li><a href="contact"> CONTACT </a></li>
        <li><a href="panier"> PANIER </a></li>

        <li><a></a></li>
        <li><a></a></li>
        <li><a></a></li>


        <li><a href="authentification"> SE CONNECTER </a></li>
        <li><a href="inscription"> S'INSCRIRE </a></li>


			</ul>




			  $content <br/>





		</section>



        <footer>
        <hr/>
			<span>Site WEB LIMAGA 2017/2018 </span><strong>Gabriel DAUBENFELD && Quentin DENOIS && Juliette KRATZ</strong>
		</footer>
    </body>

</html>
END;
echo $html;
}

}
