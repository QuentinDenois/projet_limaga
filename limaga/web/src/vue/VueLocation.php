<?php
namespace mywishlist\vue;

class VueLocation{

  private $tableau;

  public function __construct($tab){
    $this->tableau = $tab;
  }

  public function afficherListesDuCreateur(){

    $res='<section>';
    foreach($this->tableau as $t){
      $res.= 'Titre de la liste = '.$t['titre'].'<br>Description de la liste = '.$t['description'].'<br>------<br>';
    }
    $res.='</section>';
    $res .= '<form id="creerListe" method="POST" action="./listes">
    <fieldset>
      <legend>Creation d une nouvelle liste</legend>
      <label for="f1_name">Titre de la nouvelle liste : </label>
      <input type="text" id="f1_name" name="titreListe" required>
      <label for="f2_name">Description :</label>
      <input type="text" id="f2_name" name="descrListe" required>
      <label for="f3_name">Date d expiration :</label>
      <input type="date" id="f3_name" name="expiration" required>

        <button type="submit">Creer la liste</button></form>
    </fieldset>';


    return $res;
  }

  public function render(int $selecteur) {
  switch ($selecteur) {
    case 0 :
    $content = $this->afficherListesDuCreateur();
    break;
  }
$html =<<<END
<!DOCTYPE html>
<html>
<head> … </head>
<body>
…
<div class="content">
$content
</div>
</body><html>
END;
echo $html;
}


}
