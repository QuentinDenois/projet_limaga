<?php
namespace mywishlist\vue;

class VuePanier{

  private $tableau;

public function __construct($tab){
  $this->tableau = $tab;
}

private function afficherListeDesListes(){
  $res='<section>';
  foreach($this->tableau as $t){
    $res.= 'Numero de la liste : '.$t['no'].'<br>Titre de la liste : '.$t['titre'].'<br>Description de la liste : '.$t['description'].'<br>Date d expiration : '.$t['expiration'].'<br>------<br>';
  }
  $res.='</section>';
  $res .= '<form id="creerListe" method="POST" action="./listes">
  <fieldset>
    <legend>Creation d une nouvelle liste</legend>
    <label for="f1_name">Titre de la nouvelle liste : </label>
    <input type="text" id="f1_name" name="titreListe" required>
    <label for="f2_name">Description :</label>
    <input type="text" id="f2_name" name="descrListe" required>
    <label for="f3_name">Date d expiration :</label>
    <input type="date" id="f3_name" name="expiration" required>
    <button type="submit">Creer la liste</button></form>
  </fieldset>';

  $res .= '<form id="modifierListe" method="POST" action=".">
  <fieldset>
    <legend>Modification une liste</legend>
    <label for="f1_name">Numero de la liste a modifier : </label>
    <input type="number" id="f1_name" name="numeroListe" required>
    <label for="f2_name">Titre :</label>
    <input type="text" id="f2_name" name="titreListe">
    <label for="f3_name">Description :</label>
    <input type="text" id="f3_name" name="descrListe">
    <label for="f4_name">Date d expiration :</label>
    <input type="date" id="f4_name" name="expiration">
    <button type="submit">Modifier la liste</button></form>
  </fieldset>';

  return $res;
}

private function afficherItemsDeListe(){
  $res='<section>';
  //tableau contient une liste
  $res.= $this->tableau[0]['titre'].'<br> Description : '.$this->tableau[0]['description'].'<br>';
  $items = \mywishlist\models\Prestation::where('liste_id','=',$this->tableau[0]['no'])->get();

  foreach($items as $item){
    $res.= '<br> item : '.$item->nom.'<br> Réservé par : '.$item->participant.' // Message :'.$item->message.'<br>';
  }
  $res.='</section>';

  return $res;
}

private function afficherItem(){
  $res='<section>';
  foreach($this->tableau as $t){
    $res.= 'Prestation numéro '.$t['id'].'<br> Nom de l\'item = '.$t['nom'].'<br> Description de l\'item = '.$t['descr'].'<br>';
  }
//  $res.= $this->tableau['nom'].':'.$this->tableau['descr'].'<br>';
  $iditem = $this->tableau[0]['id'];
  $res .= '<form id="reserver1" method="POST" action="'.$iditem.'">
    <input type="text" placeholder="<participant>" name="participant">';

  if($this->tableau[0]['participant'] == NULL){
    $res .= '<form id="reserver1" method="POST" action="'.$iditem.'">
             <input type="text" placeholder="<message>" name="message">';
  }
  $res.='<button type="submit" name="reserver" value="Reserver_reserve1">Réserver</button>
         </form>
         </section>';
  return $res;
}

// private function afficherComptes(){
//   $res='<section>'. 'Login déjà existants = <br>';
//   foreach($this->tableau as $t){
//     $res.= $t['login'].'<br>';
//   }
//   $res.='</section>';
//
//   return $res;
// }

private function afficherItemsDePanier(){
  $res='<section>';
  //tableau contient une liste
  $res.= $this->tableau[0]['type'].'<br> Description : '.$this->tableau[0]['description'].'<br>';
  $prestation = \mywishlist\models\Prestation::where('liste_id','=',$this->tableau[0]['no'])->get();

  foreach($prestation as $prestation){
    $res.= '<br> prestation : '.$prestation->type.' : '.$item->description.'<br>';
  }
  $res.='</section>';

  return $res;
}


public function render(int $selecteur) {
switch ($selecteur) {
 case 0 :
 $content = $this->afficherListeDesListes();
 break;

 case 1 :
 $content = $this->afficherItemsDeListe();
 break;

 case 2 :
 $content = $this->afficherItem();
 break;
 case 3 :
 $content = $this->afficherPanier();
 break;
}
$html = <<<END
<!DOCTYPE html>
<html>
<head> … </head>
<body>
 …
<div class="content">
 $content
</div>
</body><html>
END;
echo $html;
}

}
