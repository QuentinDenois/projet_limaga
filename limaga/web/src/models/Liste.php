<?php
namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model{

  protected $table = 'liste';
  protected $primaryKey = 'no_liste';
  public $timestamps = false;

  public function prestations(){
      return $this->hasMany('\mywishlist\models\Prestation','liste_id');
  }
}
