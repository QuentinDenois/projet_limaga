<?php

namespace mywishlist\models;

class Prestation extends \Illuminate\Database\Eloquent\Model{

  protected $table = 'prestation';
  protected $primaryKey = 'prestationn_id';
  public $timestamps = false;

  public function liste(){
    return $this->belongsTo('\mywishlist\models\Liste','liste_id');
  }

}
